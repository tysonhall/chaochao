<?php
namespace app\common\model;

use think\Model;

class GetBsprocessLog extends Model
{
	public function index($data)
	{
		// $data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['BSRSTARTPROCESS'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = $this->insert($insertData);

		$bsprocess = model('Bsprocess')->where([['status','eq',1]])->find();

		if(!$bsprocess) {
			$result = [
				"Process" => [],
			];
		} else {
			$result = [
				"Process" => json_decode($bsprocess['process'],true),
			];
		}

		return $result;
		// $resultStr = $this->setData(json_encode($result));
		// echo $resultStr;
	}
}
