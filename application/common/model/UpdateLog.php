<?php
namespace app\common\model;

use think\Model;

class UpdateLog extends Model
{
	public function index($data)
	{
		// $data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['VERSION'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = $this->insert($insertData);

		$updateFile = model('UpdateFile')->where([['status','eq',1]])->find();

		if(!$updateFile) {
			$result = [
				"HOST" => '',
				"PATH" => '',
				"PORT" => '',
				"VERSION" => '',
				"MD5" => '',
				"KEY" => '',
			];
		} else {
			$result = [
				"HOST" => $updateFile['host'],
				"PATH" => $updateFile['path'],
				"PORT" => $updateFile['port'],
				"VERSION" => $updateFile['version'],
				"MD5" => $updateFile['md5'],
				"KEY" => $updateFile['key'],
			];
		}

		return $result;
		// $resultStr = $this->setData(json_encode($result));
		// echo $resultStr;
	}
}
