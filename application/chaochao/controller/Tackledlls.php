<?php
/**
 * 拦截DLL控制器
 */
namespace app\chaochao\controller;

class Tackledlls extends Common
{
	/**
	 * 列表
	 * @author 秋水
	 * @DateTime 2019-03-24T18:01:57+0800
	 */
	public function index()
	{
		return $this->fetch();
	}

	/**
	 * 获取拦截dll列表信息
	 * @author 秋水
	 * @DateTime 2019-03-24T18:03:02+0800
	 */
	public function tackledllsList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('Tackledlls')->where($where)->page($page, $limit)->order('id asc')->select()->toArray();
        $count = model('Tackledlls')->where($where)->count();

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 添加拦截DLL
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function addTackledlls()
	{
		if(request()->isPost()) {
			$dll = input('dll/a', '');
			$status = input('status', 0);

			$addData = [
				'dll' => json_encode($dll),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Tackledlls')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Tackledlls')->insert($addData);
			if($r) {
				return json(['code'=>0, 'msg'=>'添加成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'添加失败']);
			}
		} else {
			return $this->fetch();
		}
	}

	/**
	 * 添加拦截dll
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function editTackledlls()
	{
		if(request()->isPost()) {
			$id = input('id', 0);
			$dll = input('dll/a', []);
			$status = input('status', 0);

			$updateData = [
				'dll' => json_encode($dll),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Tackledlls')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Tackledlls')->where([['id','eq',$id]])->update($updateData);
			if($r !== false) {
				return json(['code'=>0, 'msg'=>'编辑成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'编辑失败']);
			}
		} else {
			$id = input('id', 0);

			$this->assign('id', $id);
			return $this->fetch();
		}
	}

	/**
	 * 获取拦截dll
	 * @author 秋水
	 * @DateTime 2019-03-24T22:17:21+0800
	 */
	public function getTackledlls()
	{
		$id = input('id', 0);

		$data = model('Tackledlls')->where([['id','eq',$id]])->find();
		$data['dll'] = json_decode($data['dll'],true);
		$data['process'] = json_decode($data['process'],true);

		return json(['code'=>0,'data'=>$data]);
	}

	/**
	 * 修改DLL进程
	 * @author 秋水
	 * @DateTime 2019-03-24T22:57:28+0800
	 */
	public function editTackledllsProcess()
	{
		if(request()->isPost()) {
			$id = input('id', 0);
			$process = input('process/a', []);
			$process_dll = input('process_dll/a', []);
			$status = input('status', 0);

			$processArr = [];
			foreach ($process as $key => $o) {
				$processArr[$o] = [];
				foreach ($process_dll[$key] as $key1 => $p) {
					$processArr[$o][] = $key1;
				}
			}
			$updateData = [
				'process' => json_encode($processArr),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Tackledlls')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Tackledlls')->where([['id','eq',$id]])->update($updateData);
			if($r !== false) {
				return json(['code'=>0, 'msg'=>'编辑成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'编辑失败']);
			}
		} else {
			$id = input('id', 0);
			$data = model('Tackledlls')->where([['id','eq',$id]])->find();

			$this->assign('data', $data);
			$this->assign('id', $id);
			return $this->fetch();
		}
	}

	/**
	 * 删除拦截Dll
	 * @author 秋水
	 * @DateTime 2019-03-24T18:36:06+0800
	 * @return   [type]                   [description]
	 */
	public function delTackledlls()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('Tackledlls')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}
}