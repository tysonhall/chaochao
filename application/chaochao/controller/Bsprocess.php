<?php
/**
 * 浏览器父进程控制器
 */
namespace app\chaochao\controller;

class Bsprocess extends Common
{
	/**
	 * 父进程列表
	 * @author 秋水
	 * @DateTime 2019-03-24T18:01:57+0800
	 */
	public function index()
	{
		return $this->fetch();
	}

	/**
	 * 获取父进程列表信息
	 * @author 秋水
	 * @DateTime 2019-03-24T18:03:02+0800
	 */
	public function processList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('Bsprocess')->where($where)->page($page, $limit)->order('id asc')->select()->toArray();
        $count = model('Bsprocess')->where($where)->count();

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 添加父进程
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function addProcess()
	{
		if(request()->isPost()) {
			$process = input('process/a', '');
			$status = input('status', 0);

			$addData = [
				'process' => json_encode($process),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Bsprocess')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Bsprocess')->insert($addData);
			if($r) {
				return json(['code'=>0, 'msg'=>'添加成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'添加失败']);
			}
		} else {
			return $this->fetch();
		}
	}

	/**
	 * 添加父进程
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function editProcess()
	{
		if(request()->isPost()) {
			$id = input('id', 0);
			$process = input('process/a', '');
			$status = input('status', 0);

			$updateData = [
				'process' => json_encode($process),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Bsprocess')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Bsprocess')->where([['id','eq',$id]])->update($updateData);
			if($r !== false) {
				return json(['code'=>0, 'msg'=>'编辑成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'编辑失败']);
			}
		} else {
			$id = input('id', 0);

			$this->assign('id', $id);
			return $this->fetch();
		}
	}

	/**
	 * 获取浏览器父进程
	 * @author 秋水
	 * @DateTime 2019-03-24T22:17:21+0800
	 */
	public function getProcess()
	{
		$id = input('id', 0);

		$data = model('Bsprocess')->where([['id','eq',$id]])->find();
		$data['process'] = json_decode($data['process'],true);

		return json(['code'=>0,'data'=>$data]);
	}

	/**
	 * 删除父进程
	 * @author 秋水
	 * @DateTime 2019-03-24T18:36:06+0800
	 * @return   [type]                   [description]
	 */
	public function delProcess()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('Bsprocess')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}
}