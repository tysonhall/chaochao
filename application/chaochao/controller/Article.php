<?php
namespace app\chaochao\controller;

use think\Request;

class Article extends Common
{

    /**
     * 文章列表
     * @author 雨夜
     * @DateTime 2018年11月27日09:42:11
     */
    public function index()
    {
        $keywords = input('param.keywords', '');
        $page     = input('param.page', 1);
        $limit    = input('param.limit', 10);
        $where    = [];
        $model    = model('Article');
        if ($keywords != '') {
            $where[] = ['title', 'like', "%$keywords%"];
        }
        $list = $model->where($where)->page($page, $limit)->order('create_time asc')->select();
        $this->assign('limit', $limit);
        $this->assign('page', $page);
        $this->assign('count', count($list));
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * [getArticleList 文章列表集]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function getArticleList()
    {
        $keywords = input('param.keywords', '');
        $page     = input('param.page', 1);
        $limit    = input('param.limit', 10);
        $where    = [];
        $model    = model('Article');
        if ($keywords != '') {
            $where[] = ['title', 'like', "%$keywords%"];
        }
        $list = $model->where($where)->page($page, $limit)->order('sort desc,create_time asc')->select();
        $count = $model->where($where)->count();
        return json(['code'=>0,'msg'=>'请求成功','count'=>$count,'data'=>$list]);
    }

    /**
     * 添加文章
     * @author 雨夜
     * @DateTime 2018年11月27日09:42:20
     */
    public function addArticle(Request $request)
    {
        if ($request->isAjax()) {
            $param                = input('post.');
            $model                = model('Article');
            $param['create_time'] = $param['update_time'] = time();
            $r                    = $model->insert($param);
            if( false !== $r) {
                return json(['code'=>0,'msg'=>'请求成功','desc'=>'添加成功']);
            } else {
                return json(['code'=>44005,'msg'=>'创建失败','desc'=>'添加失败']);
            }
        } else {
            return $this->fetch();
        }
    }

    /**
     * 修改文章
     * @author 雨夜
     * @DateTime 2018年11月27日09:42:27
     */
    public function editArticle(Request $request)
    {
        if ($request->isAjax()) {
            $param                = input('post.');
            $param['update_time'] = time();
            $r                    = model('Article')->update($param);
            if ($r) {
                return json(['code'=>0,'msg'=>'请求成功','desc'=>'编辑成功']);
            } else {
                return json(['code'=>44003,'msg'=>'修改失败','desc'=>'修改失败']);
            }
        } else {
            $id    = input('id', 0);
            $model = model('Article');
            $data  = $model->where([['id', 'eq', $id]])->find();
            $this->assign('data', $data);
            return $this->fetch();
        }
    }

    /**
     * [delArticle 删除文章]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function delArticle()
    {
        $re = db('article')->where(['id'=>input('id')])->delete();
        if(false !== $re){
            return json(['code'=>0,'msg'=>'请求成功','desc'=>'删除成功']);
        }else{
            return json(['code'=>44004,'msg'=>'删除失败','desc'=>'系统错误']);
        }
    }

    /**
     * [getArticleInfo 获取文章详情]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function getArticleInfo()
    {
        $param = input();
        $info = model('Article')->get($param['id']);
        if(!$info){
            return json(['code'=>44001,'msg'=>'参数错误']);
        }else{
            return json(['code'=>0,'msg'=>'请求成功','data'=>$info]);
        }
    }

}
