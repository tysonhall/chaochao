<?php
/**
 * Created by PhpStorm.
 * User: SRC
 * Date: 2019/3/18
 * Time: 14:12
 */
namespace app\chaochao\controller;

use think\Db;

class Member extends Common
{
    /**
     * [userlist 会员列表]
     * @Author   雨夜
     * @DateTime 2019-3-18 14:13:27
     * @return   [view]     [视图渲染]
     */
    public function index()
    {
        $role = db('AuthGroup')->where(['status'=>1])->select();
        $this->assign('roles',$role);
        return $this->fetch();
    }

    /**
     * [getUserList 获取会员列表]
     * @Author   雨夜
     * @DateTime 2019-01-08
     * @return   [type]     [description]
     */
    public function getUserList()
    {
        $map = [];
        $userList = model('Member')->where($map)->page(input('page'), input('limit'))->order('create_time desc')->select();
        $result['data']  = $userList;
        $result['count'] = model('Member')->count();
        $result          = array_merge($this->api_code[0], $result);

        return json($result);
    }

    /***
     * 根据ID获取用户的信息
     * @author 江远
     */
    public function getUserInfoById()
    {
        $uid = input('uid');
        if (empty($uid)) {
            $this->api_code[40001]['msg'] = '参数不正确！';
            return json($this->api_code[40001]);
        }
        $where[]        = array('id', 'eq', $uid);
        $info           = model('Member')->where($where)->find();
        if ($info) {
            $this->api_code[0]['msg']  = '获取用户信息成功！';
            $this->api_code[0]['data'] = $info;
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '获取用户信息失败！';
            return json($this->api_code[40001]);
        }
    }
}