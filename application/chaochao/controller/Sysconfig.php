<?php
namespace app\chaochao\controller;

class Sysconfig extends Common
{

	/**
	 * [index 站点配置]
	 * @Author   雨夜
	 * @DateTime 2018-12-17
	 * @return   [type]     [description]
	 */
	public function index(){
        $setfile='../config/site.php';
        if(!file_exists($setfile)){
        	fopen($setfile,"a");
		}
        $site = require $setfile;
        $this->assign('site',$site);
        return $this->fetch();
    }

    /**
     * [saveConfig 站点配置]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [view]     [description]
     */
    public function saveConfig()
    {
        $post = input();
        // if(isset($post['third_login'])){
        //     $post['third_login'] = 1;
        // }else{
        //     $post['third_login'] = 0;
        // }
        $setfile='../config/site.php';
        $set=var_export($post, TRUE);
        $settingstr="<?php \n return ";
        $settingstr.=$set;
        $settingstr.=";\n?>\n";
        if(file_put_contents($setfile,$settingstr)>0){
            return json(['code'=>0,'msg'=>'请求成功','desc'=>'保存成功']);
        }else{
            return json(['code'=>44003,'msg'=>'修改失败','desc'=>'保存失败']);
        }
    }

    /**
     * [contacts 客服列表]
     * @Author   雨夜
     * @DateTime 2018-12-17
     * @return   [type]     [description]
     */
    public function contacts()
	{
		return  view();
	}
}
?>