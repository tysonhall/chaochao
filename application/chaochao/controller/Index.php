<?php
/**
 * 秋水
 * 2018/11/28
 * 默认执行控制类
 */
namespace app\chaochao\controller;
use think\Db;
class Index extends Common
{
    public function initialize(){
        parent::initialize();
    }

	/**
	 * 首页渲染
	 * @author 秋水
	 * @DateTime 2018-11-29T14:40:43+0800
	 */
    public function index()
    {
    	return $this->fetch();
    }

    /**
     * 首页控制台
     * @author 秋水
     * @DateTime 2018-11-29T14:41:02+0800
     */
    public function controlBoard()
    {
    	return $this->fetch();
    }
}
