<?php
/**
 * 配置控制器
 */
namespace app\chaochao\controller;

class Config extends Common
{
	/**
	 * 配置列表
	 * @author 秋水
	 * @DateTime 2019-03-24T18:01:57+0800
	 */
	public function index()
	{
		return $this->fetch();
	}

	/**
	 * 获取配置列表信息
	 * @author 秋水
	 * @DateTime 2019-03-24T18:03:02+0800
	 */
	public function configList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('Config')->where($where)->page($page, $limit)->order('id asc')->select()->toArray();
        $count = model('Config')->where($where)->count();

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 添加配置
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function addConfig()
	{
		if(request()->isPost()) {
			$CHANGEPARENT = input('CHANGEPARENT/a', []);
			$domain = input('domain/a', []);
			$path = input('path/a', []);
			$process = input('process/a', []);
			$desktopshortcut_name = input('desktopshortcut_name/a', []);
			$desktopshortcut_domain = input('desktopshortcut_domain/a', []);
			$iefavorites_name = input('iefavorites_name/a', []);
			$iefavorites_domain = input('iefavorites_domain/a', []);
			$status = input('status', 0);

			$shpArr = [];
			foreach ($domain as $key => $o) {
				$shpArr[$o] = [];
				foreach ($process[$key] as $key1 => $p) {
					$arr = [
						$p => $path[$key][$key1],
						'CHANGEPARENT' => $CHANGEPARENT[$key][$key1],
					];
					$shpArr[$o][] = $arr;
				}
			}

			$dtscArr = [];
			foreach ($desktopshortcut_name as $key => $o) {
				$dtscArr[$o] = $desktopshortcut_domain[$key];
			}

			$iefArr = [];
			foreach ($iefavorites_name as $key => $o) {
				$iefArr[$o] = $iefavorites_domain[$key];
			}

			$addData = [
				'shp' => json_encode($shpArr),
				'dtsc' => json_encode($dtscArr),
				'ief' => json_encode($iefArr),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Config')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Config')->insert($addData);
			if($r) {
				return json(['code'=>0, 'msg'=>'添加成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'添加失败']);
			}
		} else {
			return $this->fetch();
		}
	}

	/**
	 * 添加配置
	 * @author 秋水
	 * @DateTime 2019-03-24T18:15:40+0800
	 */
	public function editConfig()
	{
		if(request()->isPost()) {
			$id = input('id', 0);
			$CHANGEPARENT = input('CHANGEPARENT/a', []);
			$domain = input('domain/a', []);
			$path = input('path/a', []);
			$process = input('process/a', []);
			$desktopshortcut_name = input('desktopshortcut_name/a', []);
			$desktopshortcut_domain = input('desktopshortcut_domain/a', []);
			$iefavorites_name = input('iefavorites_name/a', []);
			$iefavorites_domain = input('iefavorites_domain/a', []);
			$status = input('status', 0);

			$shpArr = [];
			foreach ($domain as $key => $o) {
				$shpArr[$o] = [];
				foreach ($process[$key] as $key1 => $p) {
					$arr = [
						$p => $path[$key][$key1],
						'CHANGEPARENT' => $CHANGEPARENT[$key][$key1],
					];
					$shpArr[$o][] = $arr;
				}
			}

			$dtscArr = [];
			foreach ($desktopshortcut_name as $key => $o) {
				$dtscArr[$o] = $desktopshortcut_domain[$key];
			}

			$iefArr = [];
			foreach ($iefavorites_name as $key => $o) {
				$iefArr[$o] = $iefavorites_domain[$key];
			}

			$updateData = [
				'shp' => json_encode($shpArr),
				'dtsc' => json_encode($dtscArr),
				'ief' => json_encode($iefArr),
				'status' => $status,
			];

			if($status == 1) {
				// 如果配置信息为“启用”状态，则关闭其他已启用的配置
				model('Config')->where([['status','eq',1]])->update(['status'=>0]);
			}

			$r = model('Config')->where([['id','eq',$id]])->update($updateData);
			if($r !== false) {
				return json(['code'=>0, 'msg'=>'编辑成功']);
			} else {
				return json(['code'=>40001, 'msg'=>'编辑失败']);
			}
		} else {
			$id = input('id', 0);

			$this->assign('id', $id);
			return $this->fetch();
		}
	}

	/**
	 * 获取配置详情
	 * @author 秋水
	 * @DateTime 2019-03-24T21:22:22+0800
	 */
	public function getConfig()
	{
		$id = input('id', 0);
		$data = model('Config')->where([['id','eq',$id]])->find();

		$data['shp'] = json_decode($data['shp']);
		$data['dtsc'] = json_decode($data['dtsc']);
		$data['ief'] = json_decode($data['ief']);

		return json(['code'=>0,'data'=>$data]);
	}

	/**
	 * 删除配置
	 * @author 秋水
	 * @DateTime 2019-03-24T18:36:06+0800
	 */
	public function delConfig()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('Config')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}
}