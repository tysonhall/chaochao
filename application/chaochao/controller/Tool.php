<?php
/**
 * 工具控制类
 */
namespace app\chaochao\controller;

use extend\Aes;
class Tool extends Common
{
	/**
	 * 加密解密测试
	 * @author 秋水
	 * @DateTime 2019-03-24T07:40:50+0800
	 */
	public function AesTest()
	{
		if(request()->isPost()) {
			$type = input('type', '');
			$text = input('text', '');

			if($text == '') {
				return json(['code'=>40001,'msg'=>'参数不能为空']);
			}

			$Aes = new Aes();
			$key1Arr = [0xBB, 0x94, 0x79, 0xF1, 0xBB, 0x93, 0x78, 0xF1, 0xB9, 0x92, 0x77, 0xF1, 0xB8, 0x91, 0x77, 0xF0, 0xB8, 0x91, 0x77, 0xEF, 0xB7, 0x90, 0x75, 0xEF, 0xB6, 0x8E, 0x74, 0xEF, 0xB6, 0x8D, 0x73, 0xEF];
			$key2Arr = [0xB5, 0x8C, 0x73, 0xEF, 0xB5, 0x8A, 0x71, 0xEF, 0xB4, 0x89, 0x71, 0xEF, 0xB3, 0x88, 0x6F, 0xEF, 0xB3, 0x88, 0x6F, 0xEF, 0xB2, 0x86, 0x6E, 0xEF, 0xB2, 0x85, 0x6D, 0xEF, 0xB1, 0x84, 0x6C, 0xEF];
			$key1 = $this->arrToStr($key1Arr);
			$key2 = $this->arrToStr($key2Arr);

			if($type == 'encrypt') {
				$resultStr = $Aes->encrypt($text, $key1, $key2);
			} else {
				$resultStr = $Aes->decrypt($text, $key1, $key2);
			}

			$result = ['code'=>0,'msg'=>'加密成功','data'=>$resultStr];

			return json($result);
		} else {
			return $this->fetch();
		}
	}

	/**
	 * key数组转化为字符串
	 * @author 秋水
	 * @DateTime 2019-03-23T22:36:49+0800
	 */
	public function arrToStr($arr) {
		$str = '';
		foreach ($arr as $key => $o) {
			$str .= chr(hexdec($o));
		}
		return $str;
	}
}