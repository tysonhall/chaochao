<?php
/**
 * 秋水
 * 获取配置
 */
namespace app\api\controller;

use think\Request;

class GetConfig extends Common
{
	/**
	 * 默认执行方法
	 * @author 秋水
	 * @DateTime 2019-03-24T08:12:43+0800
	 */
	public function index()
	{
		$data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['CONFIG'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = model('GetConfigLog')->insert($insertData);

		$config = model('Config')->where([['status','eq',1]])->find();

		if(!$config) {
			$result = [
				"SETHOMEPAGE" => [],
				"DESKTOPSHORTCUT" => [],
				"IEFAVORITES" => [],
			];
		} else {
			$result = [
				"SETHOMEPAGE" => json_decode($config['shp'],true),
				"DESKTOPSHORTCUT" => json_decode($config['dtsc'],true),
				"IEFAVORITES" => json_decode($config['ief'],true),
			];
		}

		$resultStr = $this->setData(json_encode($result));
		echo $resultStr;
	}
}