<?php
/**
 * 秋水
 * 获取配置
 */
namespace app\api\controller;

use think\Request;

class GetTackledlls extends Common
{
	/**
	 * 默认执行方法
	 * @author 秋水
	 * @DateTime 2019-03-24T08:12:43+0800
	 */
	public function index()
	{
		$data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['TACKLEDLLS'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = model('TackledllsLog')->insert($insertData);

		$tackledlls = model('Tackledlls')->where([['status','eq',1]])->find();

		if(!$tackledlls) {
			$result = [
				"dll" => [],
				"process" => [],
			];
		} else {
			$result = [
				"dll" => json_decode($tackledlls['dll'],true),
				"process" => json_decode($tackledlls['process'],true),
			];
		}

		$resultStr = $this->setData(json_encode($result));
		echo $resultStr;
	}
}