<?php
/**
 * 秋水
 * 默认
 */
namespace app\api\controller;

use think\Request;

class Index extends Common
{
	public function index()
	{
		$path = request()->path();
		$controller = $this->getController($path);

		$data = $this->getData();
		$r = model($controller)->index($data);
		if($r) {
			$resultStr = $this->setData(json_encode($r));
			echo $resultStr;
		}
	}
}